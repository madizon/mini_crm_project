<?php

return [

    'dashboard' => 'Tableau de bord',
    'companies' => 'Entreprises',
    'employees' => 'Employées',
    'moreInfo' => 'Plus d`informations',
    'data' => 'Les données',
    'copyright' => 'Copyright © 2014-2019 AdminLTE.io. Tous les droits sont réservés.',


    'name' => 'Nom',
    'email' => 'Email',
    'logo' => 'Logo',
    'website' => 'Website',
    'action' => 'Action',
    'company' => 'Compagnie',
    'contactNo' => 'No de contact',
    'firstName' => 'Prénom',
    'lastName' => 'Nom de famille',
    'created_at' => 'Établi',
    'updated_at' => 'Actualisé',
    'createNewCompany' => 'Créer une nouvelle entreprise',
    'createNewEmployee' => 'Créer un nouvel employé',
    'addNewCompany' => 'Ajouter une nouvelle entreprise',
    'addNewEmployee' => 'Ajouter un nouvel employé',
    'editCompany' => 'Modifier l`entreprise',
    'editEmployee' => 'Modifier l`employé',
    'submit' => 'Soumettre',
    'show' => 'Montrer',
    'edit' => 'Éditer',
    'update'=> 'Mettre à jour',
    'delete' => 'Effacer', 
    'created' => 'Établi',
    'lastUpdated'=> 'Dernière mise à jour',
    'chooseFile'=> 'Choisir le fichier',
    'uploadedLogo'=> 'Logo téléchargé',
    'uploadNewLogo'=> 'Télécharger un nouveau logo',

    'confirmDelete'=> 'Etes-vous sûr que vous voulez supprimer?',
    'confirmCompanyDelete'=> 'Cela peut également supprimer un employé de cette société. Etes-vous sûr que vous voulez supprimer?',
    'confirmUpdate'=> 'Voulez-vous vraiment mettre à jour?',

    'addedSuccessfully' => 'Ajouté avec succès',
    'deletedSuccessfully' => 'Supprimé avec succès',
    'updatedSuccessfully' => 'Mis à jour avec succés',
];
