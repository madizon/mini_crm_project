@extends('layouts.app')

@section('content')
<div class="container-fluid">
<h5 class="mb-2 mt-4"></h5>

          <div class="col-md-6">
            <div class="card card-widget">
              <div class="card-header">
                  <span><h4>{{ $employee->first_name . ' ' . $employee->last_name }}</h4></span>
              </div>

              <div class="card-body">
                <span><h6>{{ __('label.company') }}: <b>{{ $employee->company->name }}</b></h6></span>
                <span><h6>{{ __('label.email') }}: <b>{{ $employee->email }}</b></h6></span>
                <span><h6>{{ __('label.contactNo') }}:  <b>{{ $employee->phone }}</b></h6></span>                
                <br>
                <small>{{ __('label.created') }}: {{ $employee->created_at }}</small>
                <br>
                <small>{{ __('label.lastUpdated') }}: {{ $employee->updated_at }}</small>
              </div>
            </div>
          </div>
</div>
@endsection

