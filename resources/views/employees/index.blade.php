@extends('layouts.app')

@section('content')
<div class="container-fluid">
<br>
<h5 class="mb-2">{{ __('label.employees') }}</h5>

  <div class="pull-right">
                  <a class="btn btn-success" href="{{ route('employees.create') }}"> Create New Employee</a>
  </div>

  <br>

    <form method="GET">
      <div class="form-inline">
        <label>Number of Rows:</label>&nbsp;
        <input type="number" class="form-control col-md-1 changePagination" name="numrows" value="{{ isset($request->numrows) ? $request->numrows : 10 }}" >
      </div>  
      
      <div class="form-inline">
           <label>Search By:</label>&nbsp;
           <select class="form-control col-md-2" name="by" id="searchby">         
                <option style="display:none"></option>  
                <option value="first_name" {{ isset($request->by) ? ($request->by == 'first_name' ? 'selected' : '') : '' }} >First Name</option>
                <option value="last_name" {{ isset($request->by) ? ($request->by == 'last_name' ? 'selected' : '') : '' }} >Last Name</option>
                <option value="company" {{ isset($request->by) ? ($request->by == 'company' ? 'selected' : '') : '' }} >Company</option>
                <option value="email" {{ isset($request->by) ? ($request->by == 'email' ? 'selected' : '') : '' }} >Email</option>
                <option value="phone" {{ isset($request->by) ? ($request->by == 'phone' ? 'selected' : '') : '' }} >Contact No.</option>
                <option value="created_at" {{ isset($request->by) ? ($request->by == 'created_at' ? 'selected' : '') : '' }} >Created</option>
                <option value="updated_at" {{ isset($request->by) ? ($request->by == 'updated_at' ? 'selected' : '') : '' }} >Updated</option>
            </select>
            &nbsp;

            &nbsp;
           <input type="text" name="datefilter" class="form-control col-md-4" id="reservationtime" value="{{ isset($request->datefilter) ? $request->datefilter : '' }}" style="display:none;">

            &nbsp;
           <input type="text" name="value" class="form-control col-md-3" id="searchValue" value="{{ isset($request->value) ? $request->value : '' }}"> 
           
            &nbsp;
           <input type="submit" value="Search" class="btn btn-block btn-info col-md-1">
      </div>
    </form>


                <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>{{ __('label.firstName') }}</th>
                    <th>{{ __('label.lastName') }}</th>
                    <th>{{ __('label.company') }}</th>
                    <th>{{ __('label.email') }}</th> 
                    <th>{{ __('label.contactNo') }}</th>   
                    <th>{{ __('label.created_at') }}</th> 
                    <th>{{ __('label.updated_at') }}</th>    
                    <th>{{ __('label.action') }}</th>            
                  </tr>
                  </thead>
                  <tbody>
                  @if(is_null($employees))
                    </tbody>                  
                    </table>
                  @else
                    @foreach($employees as $employee)
                    <tr>
                      <td>{{ $employee->first_name }}</td>
                      <td>{{ $employee->last_name }}</td>
                      <td>{{ $employee->company->name }}</td>
                      <td>{{ $employee->email }}</td>
                      <td>{{ $employee->phone }}</td>
                      <td>{{ date('d-M-Y h:i:s A', strtotime($employee->created_at)) }}</td>
                      <td>{{ date('d-M-Y h:i:s A', strtotime($employee->updated_at)) }}</td>
                      <td>  
                      <form action="{{ route('employees.destroy', $employee->id) }}" method="POST" onSubmit="return confirm('{{ __('label.confirmDelete') }}');">
                          <a class="btn btn-info" href="{{ route('employees.show', $employee->id) }}"> {{ __('label.show') }}</a>  
                          <a class="btn btn-primary" href="{{ route('employees.edit', $employee->id) }}"> {{ __('label.edit') }}</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">{{ __('label.delete') }}</button>
                      </form>
                      </td>
                    </tr> 
                    @endforeach              
                  </tbody>                  
                </table>
                <div>Showing 
                 {{ ($employees->currentpage()-1) * $employees->perpage()+1}} to {{ $employees->currentpage() * $employees->perpage()}}
                   of  {{ $employees->total() }} entries
                </div>
                <div class="d-flex justify-content-center">
                  {!! $employees->appends(request()->query())->links() !!}    
                </div>
              @endif  
  
</div>
@endsection


