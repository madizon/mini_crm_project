@extends('layouts.app')

@section('content')
<div class="container-fluid">
<br>             

<div class="d-flex justify-content-center">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('label.addNewEmployee') }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('employees.store')}}">
              @csrf
              
                <div class="card-body">
                  @if ($errors->any())
                    <div class="form-group alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>                           
                  @endif

                  <div class="form-group">
                    <label for="InputFirstName">{{ __('label.firstName') }}</label>
                    <input type="text" class="form-control" name="first_name" id="InputFirstName" placeholder="{{ __('label.firstName') }}" value="{{ old('first_name') }}" maxlength="255"> 
                  </div>
                  <div class="form-group">
                    <label for="InputLastName">{{ __('label.lastName') }}</label>
                    <input type="text" class="form-control" name="last_name" id="InputLastName" placeholder="{{ __('label.lastName') }}" value="{{ old('last_name') }}" maxlength="255"> 
                  </div>
                  <div class="form-group">
                    <label for="InputCompany">{{ __('label.company') }}</label>
                      <select name="company_id" class="form-control" id="SelectCompany" required>
                        <option style="display:none"></option>
                        @foreach($companies as $company)
                        <option value="{{ $company->id }}" {{ (old('company_id') == $company->id ? "selected":"") }}>{{ $company->name }}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="InputEmail">{{ __('label.email') }}</label>
                    <input type="text" class="form-control" name="email" id="InputEmail" placeholder="{{ __('label.email') }}" value="{{ old('email') }}" maxlength="255">
                  </div>
                  <div class="form-group">
                    <label for="InputPhone">{{ __('label.contactNo') }}</label>
                    <input type="text" class="form-control" name="phone" id="InputPhone" placeholder="Phone"  value="{{ old('phone') }}" maxlength="255">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">{{ __('label.submit') }}</button>
                </div>
              </form>
            </div>

  


</div>
</div>
@endsection

