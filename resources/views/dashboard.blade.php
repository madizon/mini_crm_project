@extends('layouts.app')

@section('content')
<div class="container-fluid">
<h5 class="mb-2">DASHBOARD</h5>
        <div class="row">
          <div class="col-lg-6 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $companyCount }}</h3>
                <p>{{ __('label.companies') }}</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <a href="{{ route('companies.index') }}" class="small-box-footer">
                {{ __('label.moreInfo') }} <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>

          <div class="col-lg-6 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $employeeCount }}</h3>
                <p>{{ __('label.employees') }}</p>
              </div>
              <div class="icon">
                <i class="fas fa-user"></i>
              </div>
              <a href="{{ route('employees.index') }}" class="small-box-footer">
              {{ __('label.moreInfo') }} <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>


         
        </div>
</div>
@endsection