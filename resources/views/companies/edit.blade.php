@extends('layouts.app')

@section('content')
<div class="container-fluid">
<br>
<div class="d-flex justify-content-center">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('label.editCompany') }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('companies.update', $company->id)}}" enctype="multipart/form-data" onSubmit="return confirm('{{ __('label.confirmUpdate') }}');">
              @csrf
              @METHOD('PUT')
                <div class="card-body">

                @if ($errors->any())
                    <div class="form-group alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>                           
                  @endif

                  <div class="form-group">
                    <label for="InputName">{{ __('label.name') }}</label>
                    <input type="text"  value="{{ old('name',  $company->name) }}" class="form-control" name="name" id="InputName" placeholder="Name" maxlength="255"> 
                  </div>
                  <div class="form-group">
                    <label for="InputEmail">{{ __('label.email') }}</label>
                    <input type="text" value="{{ old('email',  $company->email) }} " class="form-control" name="email" id="InputEmail" placeholder="Enter email" maxlength="255">
                  </div>
                  <div class="form-group">
                    <label for="InputEmail">{{ __('label.uploadedLogo') }}</label><br>
                    <image src="{{ asset('/storage/'.$company->logo)}}" height="100" width="100"/>
                  </div>
                  <div class="form-group">
                    <label for="InputLogo">{{ __('label.uploadNewLogo') }}</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="hidden" value="{{ $company->logo }}" name="logo">
                        <input type="file" class="custom-file-input" name="image" id="InputLogo" accept="image/*">
                        <label class="custom-file-label" for="InputLogo">{{ __('label.chooseFile') }}</label>
                      </div>                    
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="InputWebsite">{{ __('label.website') }}</label>
                    <input type="text" value="{{ old('website', $company->website) }}" class="form-control" name="website" id="InputWebsite" placeholder="Website" maxlength="255">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">{{ __('label.update') }}</button>
                </div>
              </form>
            </div>
</div>

  
</div>
@endsection

