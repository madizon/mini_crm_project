@extends('layouts.app')

@section('content')
<div class="container-fluid">
<br>
<h5 class="mb-2"> {{ __('label.companies') }}</h5>

  <div class="pull-right">
                <a class="btn btn-success" href="{{ route('companies.create') }}"> {{ __('label.createNewCompany') }}</a>
  </div>

  <br>
    <form method="GET">
      <div class="form-inline">
         <label>Number of Rows:</label>&nbsp;
         <input type="number" class="form-control col-md-1 changePagination" name="numrows" value="{{ isset($request->numrows) ? $request->numrows : 10 }}" >
      </div>      
      <div class="form-inline">
           <label>Search By:</label>&nbsp;
            <select class="form-control col-md-2" name="by" id="searchby">                         
                <option style="display: none;"></option>
                <option value="name" {{ isset($request->by) ? ($request->by == 'name' ? 'selected' : '') : '' }}>Name</option>
                <option value="email" {{ isset($request->by) ? ($request->by == 'email' ? 'selected' : '') : '' }}>Email</option>
                <option value="website" {{ isset($request->by) ? ($request->by == 'website' ? 'selected' : '') : '' }}>Website</option>
                <option value="created_at" {{ isset($request->by) ? ($request->by == 'created_at' ? 'selected' : '') : '' }}>Created</option>
                <option value="updated_at" {{ isset($request->by) ? ($request->by == 'updated_at' ? 'selected' : '') : '' }}>Updated</option>
            </select>

            &nbsp;
           <input type="text" name="datefilter" class="form-control col-md-4" id="reservationtime" value="{{ isset($request->datefilter) ? $request->datefilter : '' }}" style="display:none;">

            &nbsp;
           <input type="text" name="value" class="form-control col-md-3" id="searchValue" value="{{ isset($request->value) ? $request->value : '' }}"> 
           
            &nbsp;
          <input type="submit" value="Search" class="btn btn-block btn-info col-md-1">
      </div>
    </form>


                <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th> {{ __('label.name') }}</th>
                    <th> {{ __('label.email') }}</th>
                    <th> {{ __('label.logo') }}</th>
                    <th> {{ __('label.website') }}</th> 
                    <th> {{ __('label.created_at') }}</th>     
                    <th> {{ __('label.updated_at') }}</th> 
                    <th> {{ __('label.action') }}</th>              
                  </tr>
                  </thead>
                  <tbody>
                  @if(is_null($companies))
                    </tbody>                  
                    </table>
                  @else
                    @foreach($companies as $company)
                    <tr>
                      <td>{{ $company->name }}</td>
                      <td>{{ $company->email }}</td>
                      <td><image src="{{ asset('/storage/'.$company->logo)}}" height="100" width="100"/></td>
                      <td>{{ $company->website }}</td>
                      <td>{{ date('d-M-Y h:i:s A', strtotime($company->created_at)) }}</td>
                      <td>{{ date('d-M-Y h:i:s A', strtotime($company->updated_at)) }}</td>
                      <td>  
                      <form action="{{ route('companies.destroy', $company->id) }}" method="POST" onSubmit="return confirm('{{ __('label.confirmCompanyDelete') }}');">
                          <a class="btn btn-info" href="{{ route('companies.show', $company->id) }}">  {{ __('label.show') }}</a>  
                          <a class="btn btn-primary" href="{{ route('companies.edit', $company->id) }}">  {{ __('label.edit') }}</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger"> {{ __('label.delete') }}</button>
                      </form>
                      </td>
                    </tr> 
                    @endforeach   
                     
                  </tbody>                  
                </table>
                <div>
                
                Showing 
                 {{ ($companies->currentpage()-1) * $companies->perpage()+1}} to {{ $companies->currentpage() * $companies->perpage()}}
                   of  {{ $companies->total() }} entries
 
                </div>
                <div class="d-flex justify-content-center">
                    {!! $companies->appends(request()->query())->links() !!}            
                </div>
                @endif  
</div>
@endsection

