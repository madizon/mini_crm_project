@extends('layouts.app')

@section('content')
<div class="container-fluid">
<h5 class="mb-2 mt-4"></h5>

          <div class="col-md-6">
            <div class="card card-widget">
              <div class="card-header">
                  <span><h4>{{ $company->name }}</h4></span>
              </div>

              <div class="card-body">
                <span><h6>{{ __('label.email') }}: <b>{{ $company->email }}</b></h6></span>
                <span><h6>{{ __('label.website') }}:  <b>{{ $company->website }}</b></h6></span>
                <span><h6>{{ __('label.logo') }}:</h6></span>
                <img class="img-fluid pad" src="{{ asset('/storage/'.$company->logo)}}" alt="Photo">
                <br><br>
                <small>{{ __('label.created') }}: {{ $company->created_at }}</small>
                <br>
                <small>{{ __('label.lastUpdated') }}: {{ $company->updated_at }}</small>
              </div>
            </div>
          </div>
</div>
@endsection

