@extends('layouts.app')

@section('content')
<div class="container-fluid">
<br>
<div class="d-flex justify-content-center">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ __('label.addNewCompany') }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('companies.store')}}" enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                
                  @if ($errors->any())
                    <div class="form-group alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>                           
                  @endif

                  <div class="form-group">
                    <label for="InputName">{{ __('label.name') }}</label>
                    <input type="text" class="form-control" name="name" id="InputName" placeholder="{{ __('label.name') }}" value="{{ old('name') }}" maxlength="255"> 
                  </div>
                  <div class="form-group">
                    <label for="InputEmail">{{ __('label.email') }}</label>
                    <input type="text" class="form-control" name="email" id="InputEmail" placeholder="{{ __('label.email') }}" value="{{ old('email') }}" maxlength="255">
                  </div>
                  <div class="form-group">
                    <label for="InputLogo">{{ __('label.logo') }}</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="hidden" name="logo">
                        <input type="file" class="custom-file-input" name="image" id="InputLogo" accept="image/*">
                        <label class="custom-file-label" for="InputLogo">{{ __('label.chooseFile') }}</label>
                      </div>                    
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="InputWebsite">{{ __('label.website') }}</label>
                    <input type="text" class="form-control" name="website" id="InputWebsite" placeholder="{{ __('label.website') }}" value="{{ old('website') }}" maxlength="255">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">{{ __('label.submit') }}</button>
                </div>
              </form>
            </div>
</div>

  
</div>
@endsection

