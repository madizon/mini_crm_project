@if (Session::get('success') == "Added Successfully")
<script>
      toastr.success('{{ __('label.addedSuccessfully') }}');
</script>
@endif

@if (Session::get('success') == "Updated Successfully")
<script>
      toastr.success('{{ __('label.updatedSuccessfully') }}');
</script>
@endif

@if (Session::get('success') == "Deleted Successfully")
<script>
      toastr.success('{{ __('label.deletedSuccessfully') }}');
</script>
@endif
                         
{{ Session::forget('success') }}


