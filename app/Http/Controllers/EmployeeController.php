<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Database\Eloquent\Builder;
use DB;
use App\Employee, App\Company;
use Redirect;


class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('api');
    }
    
    public function index(Request $request)
    {
        try 
        {
            if (count($request->all()) > 0) 
            {
                //request rows only
                if(isset($request->numrows) && !isset($request->by) && !isset($request->value))
                {
                        $employees = Employee::with('company')->latest()->paginate($request->numrows);            
                }
                //request filter dates    
                elseif($request->by == "created_at" || $request->by == "updated_at")
                {
                    $date = explode(",",str_replace(" - ",",",$request->datefilter));
                    $startDate = date('Y-m-d H:i:s', strtotime($date[0]));
                    $endDate = date('Y-m-d H:i:s', strtotime($date[1]));
                                    
                    $employees = Employee::whereBetween('created_at',[$startDate,$endDate])
                                ->latest()->paginate($request->numrows); 
                }  
                //request by or value is null - set to default
                elseif(is_null($request->by) || is_null($request->value))
                {    
                        $request->numrows = 10; 
                        $employees = Employee::with('company')->latest()->paginate($request->numrows);        
                }  
                //search by row, search by and value
                else
                {
                    if($request->by === "company")
                    {
                        $employees = Employee::with('company')->whereHas('company', function (Builder $q) use ($request) {
                              $q->where('name', 'LIKE', "%{$request->value}%");
                         })->latest()->paginate($request->numrows);
                    }
                    else
                    {
                        $employees = Employee::with('company')
                                            ->where($request->by, 'LIKE', "%{$request->value}%")
                                            ->latest()->paginate($request->numrows);
                    }                               
                }   

                return view('employees.index',compact('employees','request'));        
            }
            else
            {
                $employees = Employee ::with('company')->latest()->paginate(10);  
                return view('employees.index',compact('employees'));
            }         
        } 
          catch (\Throwable $th) 
        {
           $errors = ['db' => 'Internal Server Error!'];
           return Redirect::back()->withErrors($errors); 
        }           
    }

    public function show(Employee $employee)
    {
        return view('employees.show',compact('employee'));
    }

    public function create()
    {
        try 
        {
            $companies = Company::all('id','name');
            return view('employees.create',compact('companies'));
        } 
        catch (\Throwable $th) 
        {
           $errors = ['db' => 'Internal Server Error!'];
           return Redirect::back()->withErrors($errors); 
        }
       
    }
 
    public function store(EmployeeRequest $request)
    {
            try 
            {
                DB::beginTransaction();
                Employee::create($request->all());         
                DB::commit();
                return redirect()->route('employees.index')->with('success','Added Successfully');                     
            } 
            catch (\Throwable $th) 
            {
                DB::rollBack();
                $errors = ['db' => 'Internal Server Error!'];
                return Redirect::back()->withInput($request->all())->withErrors($errors);
            }
    }

    public function edit(Employee $employee)
    {
        try 
        {
            $companies = Company::all('id','name');
            return view('employees.edit',compact('employee','companies'));
        } 
        catch (\Throwable $th) 
        {
            $errors = ['db' => 'Internal Server Error!'];
            return Redirect::back()->withErrors($errors); 
        }
      
    }

    public function update(EmployeeRequest $request, Employee $employee)
    {
            try 
            {
                DB::beginTransaction();
                $employee->update($request->all());    
                DB::commit();
                return redirect()->route('employees.index')->with('success','Updated Successfully');                                    
            } 
            catch (\Throwable $th) 
            {
                DB::rollBack();
                $errors = ['db' => 'Internal Server Error!'];
                return Redirect::back()->withInput($request->all())->withErrors($errors);
            }        
    }

    public function destroy(Employee $employee)
    {                     
            try 
            {
                DB::beginTransaction();
                $employee->delete();    
                DB::commit();
                
                return redirect()->route('employees.index')->with('success','Deleted Successfully');                         
            } 
            catch (\Throwable $th) 
            {
               DB::rollBack();
               $errors = ['db' => 'Internal Server Error!'];
               return Redirect::back()->withErrors($errors); 
            }                                                                                                                                   
    }
}
