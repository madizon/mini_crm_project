<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company, App\Employee;
use Redirect;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try 
        {
            $companyCount = Company::count();
            $employeeCount = Employee::count();
            
            return view('dashboard', compact('companyCount','employeeCount'));
        }
        catch (\Throwable $th)
        {
            $errors = ['db' => 'Internal Server Error'];
            return Redirect::back()->withErrors($errors);
        }           
    }
}
