<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use DB;
use Redirect;
use Auth;
use App\Company;
use App\Employee;
use App\Mail\SendEmail;
use App\Jobs\SendEmailJob;


class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('api');
    }

    public function index(Request $request)
    {      
        try 
        {
          if(count($request->all()) > 0) //check if there is request parameters
          {
              //request rows only
              if(isset($request->numrows) && !isset($request->by) && !isset($request->value))
              {
                  $companies = Company::latest()->paginate($request->numrows);           
              }
              //request filter dates    
              elseif($request->by == "created_at" || $request->by == "updated_at")
              {
                  $date = explode(",",str_replace(" - ",",",$request->datefilter));
                  $startDate = date('Y-m-d H:i:s', strtotime($date[0]));
                  $endDate = date('Y-m-d H:i:s', strtotime($date[1]));
                                  
                  $companies = Company::whereBetween('created_at',[$startDate,$endDate])
                               ->latest()->paginate($request->numrows); 
              }  
              //request by or value is null - set to default
              elseif(is_null($request->by) || is_null($request->value))
              {    
                  $request->numrows = 10; 
                  $companies = Company::latest()->paginate($request->numrows);
              }  
              //search by row, search by and value
              else
              {                
                  $companies = Company::where($request->by,'LIKE', "%{$request->value}%")
                                      ->latest()->paginate($request->numrows);                      
              }

            return view('companies.index',compact('companies','request'));
          }
          else
          {
             $companies = Company::latest()->paginate(10);
             return view('companies.index',compact('companies'));
          }        
        } 
          catch (\Throwable $th) 
        {
          return $th->getMessage();
           $errors = ['db' => 'Internal Server Error!'];
           return Redirect::back()->withErrors($errors); 
        }      
       
    }

    public function show(Company $company)
    {
        return view('companies.show',compact('company'));      
    }

    public function create()
    {
        return view('companies.create');
    }
    
    public function uploadImage($image,$imageName)
    {     
        if (!is_null($image))
        {
          Storage::put('public/'.$imageName, file_get_contents($image));
        }
    }

    public function store(CompanyRequest $request)
    {  
      try 
      {
        DB::beginTransaction();
        $image = null;
        $imageName = null;

          if($request->file('image'))
          {
            $image = $request->file('image'); 
            $imageName = Str::random(40).'.'. $image->extension();           
          }

          $request['logo'] = $imageName; 
          Company::create($request->all());
          $this->uploadImage($image,$imageName);  
          //*uncomment onQueue for future usage of multiple queues work with priority --queue=emails,default
          SendEmailJob::dispatch($request->except(['image']));//->onQueue('emails');      
          DB::commit();         

          return redirect()->route('companies.index')->with('success','Added Successfully'); 
      } 
      catch (\Throwable $th) 
      {
          DB::rollBack();
          $errors = ['db' => 'Internal Server Error!'];
          return Redirect::back()->withInput($request->all())->withErrors($errors);
      }      
    }
    
    public function edit(Company $company)
    {        
        return view('companies.edit',compact('company'));
    }

    public function update(CompanyRequest $request,Company $company)
    {
        try 
        {          
          DB::beginTransaction();
          $image = null;

          if($request->file('image'))
            {
              $image = $request->file('image'); 
              $imageName = Str::random(40).'.'. $image->extension();           
            }
          else $imageName = $request->logo; 
          $request->merge(['logo' => $imageName]);
          $company->update($request->all());
          $this->uploadImage($image,$imageName); 
          DB::commit();

          return redirect()->route('companies.index')->with('success','Updated Successfully');
        }
        catch (\Throwable $th)
        {
          DB::rollBack();
          $errors = ['db' => 'Internal Server Error!'];
          return Redirect::back()->withInput($request->all())->withErrors($errors);
        }
    }

    public function destroy(Company $company)
    {      
        try 
        {
            DB::beginTransaction();
            Employee::where('company_id',$company->id)->delete(); 
            $company->delete();   
            DB::commit(); 
            
            return redirect()->route('companies.index')->with('success','Deleted Successfully');
        } 
        catch (\Throwable $th) 
        {
            DB::rollBack();
            $errors = ['db' => 'Internal Server Error!'];
            return Redirect::back()->withErrors($errors); 
        }          
    }
}


