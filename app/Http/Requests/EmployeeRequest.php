<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Employee;

class EmployeeRequest extends FormRequest
{
  
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {      
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'company_id' => 'required|exists:companies,id',   
            'email'=> 'email|string|max:255',
            'phone' => 'nullable|string|max:255'
        ];
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->sometimes('email', 'unique:employees', function($request)
        {
            if(isset($request->id))
            {
                //update ignore selected ID
                return Employee::where([['id','!=',$request->id],['company_id',$request->company_id]])->first();
            }    
            else
            {
                //create
                return Employee::where('company_id',$request->company_id)->first();
            }
        });

        return $validator;
    }

    public function messages()
    {
        return [
            'first_name.required' => 'First Name is required!',
            'last_name.required' => 'Last Name is required!',
            'company_id.required' => 'Company ID is required!',
            'email' => 'Please put a valid email address!',
            'email.unique' => 'Email Address already exists on selected company!',
        ];
    }
}
