<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest    
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'name' => ['required','max:255',
                        Rule::unique('companies')->where(function ($q) {
                            $q->where('deleted_at', NULL);
                        })->ignore($this->company),
            ],
            'email' => 'email|max:255',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|dimensions:max_width=100,max_height=200',
            'logo' => 'nullable|max:255',
            'website' => 'nullable|max:255',
            'created_by_id' => 'exists:users,id',   
            'updated_by_id' => 'exists:users,id',   
        ];
    }   

    public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'email' => 'Please put a valid email address!',
            'image.dimensions' => 'Image must be 100x100 pixels',
            'image.mimes' => 'File must be have .jpeg,.png,.jpg,.gif,.svg extension.',
            'name.unique' => 'Company name already exist.',
        ];
    }
}
