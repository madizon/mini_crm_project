<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Employee;
use App\User;

class EmployeesTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function actingAdmin() : void
    {
        $this->actingAs(factory(User::class)->create());
    }

    public function testIfLoggedInToSeeEmployeesList()
    {
        $response = $this->get('/employees')->assertRedirect('/login');
    }

    public function testIfAuthenticatedUserToSeeEmployeesList()
    {
        $this->actingAdmin();
        
        $response = $this->get('/employees')->assertOk();
    }   

    public function testIfEmployeeCanBeStored()
    {
        $this->actingAdmin();

        $response = $this->post(route('employees.store'), $this->data());

        $this->assertCount(0, Employee::all());
    }

    public function testDataIsRequired()
    {
        $this->actingAdmin();

        $response = $this->post(route('employees.store'), array_merge($this->data(),[
                                        'first_name'=>null,
                                        'last_name'=>null,
                                        'company_id'=>null]));

        $this->assertCount(0, Employee::all());
    }

    private function data()
    {
       return 
       [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'company_id' => 1,
       ];
    }
}
