<?php

use JeroenZwart\CsvSeeder\CsvSeeder;
use Illuminate\Validation\Rule;

class CompanySeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->file = '/database/seeds/files/companies.csv';
        $this->truncate = FALSE;
        $this->validate = [ 'name' => ['required','max:255',
                                    Rule::unique('companies')->where(function ($q) {
                                        $q->whereNull('deleted_at');
                                    }),
                            ],
                            'email' => 'email|max:255',                            
                            'logo' => 'nullable|max:255',
                            'website' => 'nullable|max:255',];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
