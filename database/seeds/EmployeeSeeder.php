<?php

use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Database\Seeder;
use App\Company;
use App\Employee;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvRecords = Reader::createFromPath('database/seeds/files/employees.csv', 'r')
             ->setHeaderOffset(0);

        foreach ($csvRecords as $key => $record) 
        {          
            $key+1;
            $query = Company::where('name',$record['company_name']);

            if($query->count() > 0)
            { 
                $companyId = $query->value('id');
                $queryCheckEmail = Employee::where([['company_id',$companyId],['email',$record['email']],['deleted_at',NULL]])->count();

                    if($queryCheckEmail == 0)
                    {
                        DB::table('employees')->insert([
                            'first_name' => $record['first_name'],
                            'last_name' => $record['last_name'],
                            'company_id' => $companyId,
                            'email' => $record['email'],
                            'phone' => $record['phone'],
                            'created_at' => (($record['created_at'] == 'NULL') ? NULL : date('d-m-y H:i:s', strtotime($record['created_at']))),
                            'updated_at' => (($record['updated_at'] == 'NULL') ? NULL : date('d-m-y H:i:s', strtotime($record['updated_at']))),
                            'deleted_at' => (($record['deleted_at'] == 'NULL') ? NULL : date('d-m-y H:i:s', strtotime($record['deleted_at']))),
                        ]); 
                    }
                    else
                    { 
                        $this->command->line('Row:'. $key .' - Email Address already exist on the selected company.');
                    }
            }
            else
            {
                $this->command->line('Row:'. $key .' - Company not exist');
            }
        }
    }
}
